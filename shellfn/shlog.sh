# Added by matyas:
# * shlog status
# * uses SHLOG_EDITOR if defined
# * output format tweaked for easy cut-n-paste into org-mode
# * command shortened to shl
# * converts '###' to 'shlog comment'
# * aliases 'on' for 'start' and 'off' for 'stop'
# * prompt shortened

####
# shlog is a bash script that allow you to record a shell session by logging
# command history and giving diff of edited files
####
# User notes:
# - Install:
# 	Load the script using 'source shlog.bash' (add it to your .bashrc
# 	to load it automatically)
# - Usage:
# 	Start a new record with 'shlog start', log file editing 
# 	with 'shlog edit <filename>', stop the recording and get the logs
# 	with 'shlog stop'
####
# shlog is Copyright (C) 2011 Luc Sarzyniec <mail@olbat.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
####

if [[ -n "${ZSH_NAME-}" ]]; then
    autoload parameter
fi

function __shlog_path {
    echo ${TMPDIR:-/tmp}/${USER}/shlog/$$
}

function __shlog_usage {
    echo "usage: shlog <command> [<param>]"
    echo "commands:"
    echo -e "\t- on|start\t\t\t: start a new recording session"
    echo -e "\t- edit <filename>\t: edit a file and record the diff"
    echo -e "\t- comment\t\t: add a comment"
    echo -e "\t- off|stop\t\t\t: stop a recording session and get the log"
    echo -e "\t- ckpt|checkpoint\t\t\t: get log and restart recording"
    echo -e "\t- status\t\t\t: ask if a session is running"
}

function __shlog_write_history {
    if [[ -n "${BASH-}" ]]; then
        history -w
    elif [[ -n "${ZSH_NAME-}" ]]; then
        fc -W
    else
        echo Unknown shell
    fi
}

function __shlog_is_session_active {
    [[ -e "$(__shlog_path)/session.org" ]]
    return $?
}


function __shlog_start {
    if [[ ! -e $(__shlog_path) ]]; then
        mkdir -p $(__shlog_path)
        chmod 0700 $(__shlog_path)
    fi

    __shlog_write_history

    (
        echo ":SHLOG:"
        printf "\n[%s] %s@%s\n" "`date +'%F %a %H:%M'`" "$USER" "$HOSTNAME"
    ) >| $(__shlog_path)/session.org
    cp $HISTFILE $(__shlog_path)/$(basename $HISTFILE)
    echo "shlog started"

    __shlog_init_pwd=${PWD}
}


function __shlog_edit {
    local filepath
    local tmpfilebase
    local realpath
    local base
    local python
    if command -v python >/dev/null 2>&1; then
        python=python
    elif command -v python3 >/dev/null 2>&1; then
        python=python3
    elif command -v python2 >/dev/null 2>&1; then
        python=python2
    elif test -x /usr/libexec/platform-python; then
        python=/usr/libexec/platform-python
    else
        echo >&2 "Can't find Python"
    fi
    filepath=$1
    tmpfilebase=$(mktemp -d $(__shlog_path)/XXXXXXXX)
    realpath=$($python -c 'import os,sys; print(os.path.realpath(sys.argv[1] or "."))' "$filepath")
    base=$(basename "$realpath")

    touch $(__shlog_path)/files
    echo $tmpfilebase $realpath >> $(__shlog_path)/files

    cp $realpath $tmpfilebase/${base}.old &>/dev/null \
        || touch $tmpfilebase/${base}.old

    ${SHLOG_EDITOR:-${EDITOR:-vi}} $realpath

    cp $realpath $tmpfilebase/${base} &>/dev/null \
        || touch $tmpfilebase/${base}
}

function shl {
    local editfiles
    local editlog
    local editlog_add
    local funcname
    local tmp

    if [[ -n "${ZSH_NAME-}" ]]; then
        unsetopt extended_history
        unsetopt hist_ignore_dups
        unsetopt hist_ignore_all_dups
    fi


    if [[ $# -lt 1 ]]; then
        __shlog_usage
    else
        case $1 in
        stat*)
            if __shlog_is_session_active; then
                echo "shlog session active"
            else
                echo "no shlog session"
            fi
            ;;
        on|star*)
            if __shlog_is_session_active; then
                echo "Session is already active, use 'stop' before starting a new one"
            else
                __shlog_start
            fi
            ;;
        of*|sto*)
            if __shlog_is_session_active; then
                __shlog_write_history

                cat $(__shlog_path)/session.org
                printf "\n"
                printf -- "Commands:\n"
                printf "#+BEGIN_SRC sh\n"
                if [[ -n ${__shlog_init_pwd} ]]; then
                    printf "cd \"%s\"\n" "${__shlog_init_pwd}"
                fi
                editfiles=0
                editlog=""
                editlog_add=""
                funcname=shl

                while read line; do
                    if [[ -n "$(echo $line | grep "^$funcname ")" ]]; then

                        case $(echo $line | cut -d' ' -f2) in
                        e*)
                            let editfiles=editfiles+1

                            tmp=$(cat $(__shlog_path)/files \
                                    | sed -n "${editfiles}p")
                            tmpfilebase=$(echo $tmp | cut -d " " -f1)
                            realfile=$(echo $tmp | cut -d " " -f2)

                            echo "### Edit $realfile, see ($editfiles)"
                            # The above part goes into the command log.
                            # Below goes into the edit log.
                            editlog="${editlog}\n"
                            editlog="${editlog}- ($editfiles) Edit "$realfile"\n"
                            editlog="${editlog}#+NAME: $(basename $realfile).shl${editfiles}.diff\n"
                            editlog="${editlog}#+BEGIN_SRC diff\n"
                            editlog_add="$(cd $tmpfilebase && \
                                           x=$(echo *.old) && \
                                           diff -U 1 -NB "${x}" "${x%.old}" )"
                            editlog="${editlog}${editlog_add}\n"
                            editlog="${editlog}#+END_SRC\n"
                            ;;
                        c*)
                            echo "### ${line#* * }"
                            ;;
                        *)
                            :
                            ;;
                        esac
                    elif [[ -n "$(echo $line | grep "^###")" ]]; then
                        echo "$line"
                    elif [[ -n "$(echo $line | grep '^#[[:digit:]]\+$')" ]]; then
                        # ^ timestamp line
                        :
                    else
                        echo "$line"
                    fi
                done < <(diff -BN $(__shlog_path)/$(basename $HISTFILE) $HISTFILE \
                    | grep '^>.*$' | sed -e 's/^> //' -e '$d')
                echo "#+END_SRC"
                if [[ -n "$editlog" ]]; then
                    echo -n -e "\nFiles:${editlog}\n"
                    #                     ^ ${editlog} already starts with \n
                fi
                echo
                echo ":END:"
                rm -Rf $(__shlog_path)
            else
                echo "No current session, use 'start' to create a new one"
            fi
            ;;
        e*)
            if __shlog_is_session_active; then
                if [[ $# -lt 2 ]]; then
                    echo "filename missing for '$1'"
                else
                    __shlog_edit $2
                fi
            else
                echo "No current session, use 'start' to create a new one"
            fi
            ;;
        ckpt|che*)
            if __shlog_is_session_active; then
                shl stop
                shl start
            else
                echo "No current session, use 'start' to create a new one"
            fi
            ;;
        c*)
            echo "comment added"
            ;;
        h*)
            __shlog_usage
            ;;
        *)
            echo "unknown command '$1'"
            ;;
        esac

    fi

}
########
