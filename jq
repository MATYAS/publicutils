#!/usr/bin/env bash

tryexec () {
	prog=$1
	shift
	[[ -x $prog && ! $0 -ef $prog ]] && exec "$prog" "$@"
}

tryexec /usr/local/bin/jq "$@" ## local or homebrew
tryexec /opt/local/bin/jq "$@" ## macports
tryexec /usr/bin/jq "$@" ## os

PATH=$PATH:${0%/*}
case $OSTYPE in
	*darwin*)	exec jq-osx-amd64 "$@"
			;;
	*linux*)	[[ `uname -p` == x86_64 ]] && exec jq-linux64 "$@"
			exec jq-linux32 "$@"
			;;
	*)		echo "dunno what to do for this OS"
			exit 1
			;;
esac

# vim:ft=sh:noet:ts=8:sw=8:sts=8
