#!/bin/bash
which vcsh &> /dev/null || { echo "vcsh not found"; exit -1; }
vcsh status gnus > /dev/null || { ret=$?; echo "gnus repo not found"; exit $ret; }

ask_yn () {
    echo "$@"
    while true; do
        read yn
        case $yn in
            ([Yy]*) return 0 ;;
            ([Nn]*) return 1 ;;
            (*) echo "Enter yes or no" ;;
        esac
    done
}


vcsh gnus pull -srecursive -Xtheirs && \
    emacs "$@" -f gnus && \
    ask_yn "Commit and push?" && \
    vcsh gnus snapshot && \
    vcsh gnus push -f

